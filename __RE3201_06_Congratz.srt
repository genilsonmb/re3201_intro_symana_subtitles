﻿1
00:00:00,099 --> 00:00:01,655
Congratulations !

2
00:00:02,156 --> 00:00:03,402
We've reached the end of the course.

3
00:00:03,590 --> 00:00:07,381
Of course, this is not the end
of symbolic analysis learning.

4
00:00:08,250 --> 00:00:10,914
We understand by now
how SMT solvers work,

5
00:00:10,939 --> 00:00:12,903
how symbolic execution work,

6
00:00:12,928 --> 00:00:14,152
and we got a lot of flags,

7
00:00:14,780 --> 00:00:17,406
but we just covered the introduction.

8
00:00:20,153 --> 00:00:23,406
With the solid ground that you
just build in this course,

9
00:00:23,500 --> 00:00:28,652
you should be ready to go
on real-world examples.

10
00:00:29,880 --> 00:00:35,425
If you are interested on bug
hunting or exploit analysis,

11
00:00:35,450 --> 00:00:43,179
go grab the next CVE and try to analyze
and reproduce using SMT solvers or angr.

12
00:00:43,204 --> 00:00:51,319
A nice public, and open-source analysis
that you can see as an example of cases

13
00:00:51,655 --> 00:00:57,379
that you can work with
is linked on the slides

14
00:00:57,379 --> 00:01:00,615
If you are more of the
kind of developing tools,

15
00:01:00,640 --> 00:01:05,153
you can also apply symbolic
analysis in your fuzzer

16
00:01:05,178 --> 00:01:09,390
in a way of creating
feedback guided coverage.

17
00:01:10,156 --> 00:01:11,810
fuzzer. For some ideas.

18
00:01:11,810 --> 00:01:14,904
Again, you can have a look at
the angr repository on GitHub.

19
00:01:15,000 --> 00:01:16,261
Look for phuzzer.

20
00:01:16,286 --> 00:01:24,060
Phuzzer is a Python wrapper for connecting
angr and symbolic execution,

21
00:01:24,072 --> 00:01:29,970
to afl, or any other fuzzer
that you prefer to work with.

22
00:01:29,970 --> 00:01:33,030
Again, this is an introductory class.

23
00:01:33,030 --> 00:01:37,403
So, you have the solid ground
to build up your skills,

24
00:01:37,500 --> 00:01:43,153
but as any other skill
in any expert area.

25
00:01:44,220 --> 00:01:45,590
For really mastering it,

26
00:01:45,590 --> 00:01:47,390
you need a lot of practice.

27
00:01:47,390 --> 00:01:50,439
So, don't stop here.

28
00:01:50,439 --> 00:01:54,810
Go look for whatever
is your gig and enjoy.

