﻿1
00:00:00,310 --> 00:00:05,642
So, now we're going to have a bit
more hands-on experience with SMT.

2
00:00:05,667 --> 00:00:11,775
And for that, I would ask
you to open the first link.

3
00:00:11,800 --> 00:00:15,085
That's what we are going
to use here in the demo,

4
00:00:15,110 --> 00:00:19,465
or you can also use the second
one, the rise4fun link.

5
00:00:19,490 --> 00:00:21,218
It's often down.

6
00:00:21,243 --> 00:00:25,237
So, that's why I prefer
using the first one.

7
00:00:25,262 --> 00:00:31,362
They are both simply online tools for
z3 where you can just write your code

8
00:00:31,387 --> 00:00:35,278
and simulate it with
a z3 background.

9
00:00:35,303 --> 00:00:43,969
z3 is one of the most used SMT solvers
for CTF and also for development,

10
00:00:43,981 --> 00:00:50,303
mostly because it's like on other continuous
development by Microsoft,

11
00:00:50,328 --> 00:00:54,162
but also because it has
python and c bindings.i

12
00:00:54,187 --> 00:00:59,517
We're going to work a lot
with the Python bindings,

13
00:00:59,529 --> 00:01:05,726
but I think it's very helpful to
understand how z3 really works

14
00:01:05,751 --> 00:01:12,500
and how the SMT language looks like
before we jump into an abstraction

15
00:01:12,501 --> 00:01:15,021
like the Python bindings.

16
00:01:15,046 --> 00:01:19,639
So, open the link, and what
we are going to do is encode

17
00:01:19,664 --> 00:01:25,005
and execute symbolically these
five lines of assembly code.

18
00:01:25,030 --> 00:01:30,743
They look very simple, but they
have some of the small specifics

19
00:01:30,768 --> 00:01:34,961
and characteristics that
I want to show you all

20
00:01:34,962 --> 00:01:40,437
how to encode in z3, or better
in SMT language.

21
00:01:40,462 --> 00:01:47,462
in your VM, you're going to find a skeleton
for the solution right here.

22
00:01:47,472 --> 00:01:51,939
As you can see, it's not
a complete solution.

23
00:01:51,939 --> 00:01:54,448
You have it just like
some of the examples,

24
00:01:54,473 --> 00:01:57,167
and then, you can go from there.

25
00:01:57,192 --> 00:02:02,284
If you prefer to trying it out before,
like by yourself,

26
00:02:02,309 --> 00:02:06,139
please, do so and press pause right now.

27
00:02:06,164 --> 00:02:12,318
If you prefer to continue doing
it with me, open the website.

28
00:02:12,343 --> 00:02:18,638
If you are coming back, we
are going to start our walkthrough.

29
00:02:18,650 --> 00:02:25,529
We are going to encode this 5 lines
of assembly code into the website.

30
00:02:25,554 --> 00:02:36,220
First, let's have a look at the skeleton it's less
than we have and the solution inside the VM,

31
00:02:36,245 --> 00:02:38,959
and we are going
to do it together.

32
00:02:38,959 --> 00:02:41,763
So, first of all, we have the set-logic.

33
00:02:41,788 --> 00:02:44,380
This is going to
set a logic type

34
00:02:44,405 --> 00:02:54,455
that SMT 2.0 is going to use to
understand the code that we are writing.

35
00:02:54,480 --> 00:02:58,254
In our case, we are using this code.

36
00:02:58,279 --> 00:03:01,982
That means quantifier-free bit vector,

37
00:03:02,007 --> 00:03:11,074
and this semantics basically define how
they are going to deal with the two's complement

38
00:03:11,086 --> 00:03:14,980
arithmetic over the bit factors of course.

39
00:03:14,980 --> 00:03:23,634
And this kind of logic allow us to model
the semantics of assembly instructions.

40
00:03:23,659 --> 00:03:27,471
That's why we're setting it
to use this specific logic.

41
00:03:27,495 --> 00:03:33,984
If you have problems that are not in assembly
and you want to try this SMT anyway,

42
00:03:34,009 --> 00:03:35,589
of course, you can.

43
00:03:35,614 --> 00:03:42,608
You can just look at the documentation and
look for logic that fits your problem.

44
00:03:42,633 --> 00:03:45,963
Other than that, I
have this set input.

45
00:03:45,988 --> 00:03:48,534
This is not important at all.

46
00:03:48,559 --> 00:03:51,920
I just like to have it for myself.

47
00:03:51,945 --> 00:03:55,761
I am using the SMT
library version 2.0,

48
00:03:55,773 --> 00:04:00,167
and that's the latest one
that I have on my computer.

49
00:04:00,192 --> 00:04:02,591
So, I always use this version.

50
00:04:02,616 --> 00:04:04,321
So, let's get started.

51
00:04:04,346 --> 00:04:11,362
The first line of code that we want
to encode is the add eax, ecx.

52
00:04:11,374 --> 00:04:19,329
To do that, we have already defined
the eax input and ecx input.

53
00:04:19,354 --> 00:04:22,435
To declare variables, you
always need to just declare-fun

54
00:04:22,460 --> 00:04:28,665
because bit vector is a function,
and then, we give a name for it.

55
00:04:28,690 --> 00:04:31,829
In our case, the eax
input or ecx input.

56
00:04:31,854 --> 00:04:35,147
It's a function, so we
need this parenthesis,

57
00:04:35,172 --> 00:04:37,844
and then, we define the type.

58
00:04:37,869 --> 00:04:41,343
It's a bit vector and the size, 32 bit.

59
00:04:41,368 --> 00:04:46,937
The size, you can see, it's just
because we have eax and ecx.

60
00:04:46,962 --> 00:04:49,510
So, we are working with
"extended" registers.

61
00:04:49,535 --> 00:04:53,547
They are all 32 bit large.

62
00:04:53,572 --> 00:04:57,418
And we can encode our first instruction.

63
00:04:57,430 --> 00:04:59,778
The SMT language looks a bit like Lisp

64
00:04:59,803 --> 00:05:04,127
So, you have a lot of
parenthesis like limiting

65
00:05:04,152 --> 00:05:07,579
and they are very important
for the parsing.

66
00:05:07,604 --> 00:05:15,616
So, it is somehow annoying and I miss,
forget and miscount a lot of parenthesis,

67
00:05:15,641 --> 00:05:18,456
so, bear with me.

68
00:05:18,481 --> 00:05:27,226
To encode the first line of the code,
we are going to use the equal,

69
00:05:27,251 --> 00:05:33,284
and then, we need a kind of a variable
to store the results of this.

70
00:05:33,309 --> 00:05:43,180
And we are doing an addition
for eax input and ecx input

71
00:05:43,205 --> 00:05:46,947
our addition is a bit vector addition.

72
00:05:46,972 --> 00:05:50,671
So, we need the bv prefix
in the add instruction.

73
00:05:50,696 --> 00:05:58,068
So that SMT knows which kind of addition we
are going to do with this to symbolic values.

74
00:05:58,080 --> 00:06:01,789
And then, we need to declare
a temporary variable

75
00:06:01,789 --> 00:06:08,557
that is going to hold the result of this
expression that we just added here.

76
00:06:08,582 --> 00:06:15,420
First of all, we need to declare this
new variable that we need to use.

77
00:06:15,445 --> 00:06:19,236
And I am lazy so I use a
lot of copy and paste.

78
00:06:19,261 --> 00:06:24,580
And this way, I'm not missing any of
the parenthesis that I always need

79
00:06:24,581 --> 00:06:26,850
and never remember to add.

80
00:06:26,874 --> 00:06:31,386
The second important thing to
remember is that this way,

81
00:06:31,411 --> 00:06:34,657
you just encoded some
kind of CPU instruction,

82
00:06:34,682 --> 00:06:37,560
but you are not doing anything with it.

83
00:06:37,585 --> 00:06:42,241
To ensure that the computer
understands that this is a constraint

84
00:06:42,266 --> 00:06:44,639
or restriction
from your problem,

85
00:06:44,640 --> 00:06:47,153
you need to add this
keyword 'assert.'

86
00:06:47,178 --> 00:06:49,777
So, that's what we are going to do.

87
00:06:49,802 --> 00:06:53,812
We are going to add the assert
keyword to our constraint.

88
00:06:53,837 --> 00:06:59,703
And this way, we just added this
constraint to the whole model

89
00:06:59,728 --> 00:07:02,403
that we are trying to build.

90
00:07:02,429 --> 00:07:05,693
So, let's go to the second line of code.

91
00:07:05,717 --> 00:07:10,572
That's shl eax, 0xc.

92
00:07:10,597 --> 00:07:19,417
So, to do that, we can again assert,

93
00:07:19,442 --> 00:07:26,533
and then, we add a place that
we need to store a variable,

94
00:07:26,558 --> 00:07:35,902
to store the results of the expression
that we are going to have here.

95
00:07:35,927 --> 00:07:42,857
And we are going to have
a shift left from eax1

96
00:07:42,882 --> 00:07:50,652
that is the previous line or previous
value that we have in eax to 12,

97
00:07:50,677 --> 00:07:53,760
that's the c in this decimals

98
00:07:53,760 --> 00:07:56,817
So, what we want to do here first is

99
00:07:56,842 --> 00:08:03,079
we need to declare 12 in a way that it
matches the same type that we have eax.

100
00:08:03,104 --> 00:08:06,824
eax is a bit vector of a 32 bit vector.

101
00:08:06,849 --> 00:08:14,209
So, what we need to do is declare
that we need a bit vector of value 12,

102
00:08:14,221 --> 00:08:18,060
and it needs to be 32-bit large.

103
00:08:18,085 --> 00:08:20,342
And then, we have eax1.

104
00:08:20,367 --> 00:08:22,195
That's right.

105
00:08:22,220 --> 00:08:30,619
And here, you will also need to tell z3
that we want to do a bit factor shift left.

106
00:08:30,644 --> 00:08:41,591
And here, we just need a new variable
to restore the next result

107
00:08:41,616 --> 00:08:46,109
So here, we now have eax2.

108
00:08:46,134 --> 00:08:52,965
What is interesting now is that we
need to have like two assertions.

109
00:08:52,990 --> 00:08:58,211
And if you have like five lines,
we are going to have like five assertions.

110
00:08:58,236 --> 00:09:01,417
I like to put everything together somehow

111
00:09:01,442 --> 00:09:03,902
because I think it looks nicer.

112
00:09:03,927 --> 00:09:10,250
And to connect these assertions, we can
use a AND connection,

113
00:09:10,251 --> 00:09:15,905
because we want to be sure that
everything is true, not just one of it.

114
00:09:15,930 --> 00:09:21,004
So, as we remember from the truth table,
if we want that,

115
00:09:21,029 --> 00:09:25,290
that this whole block of
assertions is true only.

116
00:09:25,291 --> 00:09:29,643
If all single assertions inside
of this assert block is true,

117
00:09:29,668 --> 00:09:32,498
which means we connected with 'and,'

118
00:09:32,523 --> 00:09:37,266
and this way, we can just
continue adding assertions here.

119
00:09:37,291 --> 00:09:40,321
As we know, we have five instructions,

120
00:09:40,321 --> 00:09:44,050
so, I'm going to open all the
parenthesis that I need for it.

121
00:09:44,061 --> 00:09:47,720
And let's check the next one.

122
00:09:47,720 --> 00:09:51,486
So, the next instruction is mul ecx.

123
00:09:51,511 --> 00:09:57,607
So, for encoding it, again
we need a place to store it,

124
00:09:57,632 --> 00:10:06,714
and then, we have multiplication
With ecx_in. Knowing assembly,

125
00:10:06,739 --> 00:10:10,313
we know that it's multiplying
the value from eax

126
00:10:10,338 --> 00:10:16,310
to the value that was before in the
register eax with the ecx value.

127
00:10:16,311 --> 00:10:19,109
That is to the same of the input,

128
00:10:19,134 --> 00:10:22,381
Their multiplication is going again.

129
00:10:22,406 --> 00:10:26,093
It needs to be again
a bit factor.

130
00:10:26,118 --> 00:10:29,794
And we need again a place to store

131
00:10:29,819 --> 00:10:36,820
and the results of multiplication is
going to be stored in eax register.

132
00:10:36,845 --> 00:10:42,871
So, we declare another eax register.

133
00:10:42,896 --> 00:10:47,369
So, line number four, we
have a mov instruction.

134
00:10:47,394 --> 00:10:53,782
Mov instruction seems super complicated,
but it's the easiest one to encode,

135
00:10:53,807 --> 00:10:59,324
because if you understand
what a mov instruction is,

136
00:10:59,349 --> 00:11:02,046
it's actually pretty simple.

137
00:11:02,071 --> 00:11:08,272
It's just ensuring that the edx
register is the same as the eax

138
00:11:08,297 --> 00:11:11,777
if you are moving eax to edx.

139
00:11:11,802 --> 00:11:18,788
And what we need to do
here is add again a definition

140
00:11:18,813 --> 00:11:25,328
for the edx register
that we just use it.

141
00:11:25,352 --> 00:11:30,831
And the last line is xor edx, ecx

142
00:11:30,856 --> 00:11:38,077
So again, you're going to
need a place to store it.

143
00:11:38,102 --> 00:11:43,214
We are doing xor between edx and ecx.

144
00:11:43,239 --> 00:11:49,174
This ecx value is still the input value.

145
00:11:49,199 --> 00:11:52,991
It hasn't been changed in the whole flow.

146
00:11:53,016 --> 00:11:57,941
And edx is the edx1 that we just moved,

147
00:11:57,966 --> 00:12:02,411
Again, we are doing a
bit vector operation,

148
00:12:02,436 --> 00:12:05,240
so, we need the prefix 'bv.'

149
00:12:05,240 --> 00:12:16,320
And here, we'll declare another register
where we are going to store the new value.

150
00:12:16,320 --> 00:12:18,110
And this is edx2.

151
00:12:18,135 --> 00:12:22,144
So, after declaring all the
variables that we need,

152
00:12:22,169 --> 00:12:26,677
I normally do it like I
did it right now on the go,

153
00:12:26,702 --> 00:12:31,133
because I'll never know
the amount needed.

154
00:12:31,158 --> 00:12:37,455
And now, we have encoded
all of our five instructions.

155
00:12:37,467 --> 00:12:42,745
And we connected all of the
constraints with an and,

156
00:12:42,770 --> 00:12:47,227
asserting that everything
needs to be valid

157
00:12:47,252 --> 00:12:52,820
if we want a solution for it.

158
00:12:52,845 --> 00:12:56,802
And to check if we have
a solution or not.

159
00:12:56,827 --> 00:13:01,818
For this snippet of code,
we can use the check-set.

160
00:13:01,843 --> 00:13:05,123
So we're just going
to execute this now.

161
00:13:05,148 --> 00:13:10,390
And z3 says that our problem here.

162
00:13:10,415 --> 00:13:15,398
This is sat, the snippet of code
is a satisfiable problem,

163
00:13:15,423 --> 00:13:18,884
which means it has a solution for it.

164
00:13:18,909 --> 00:13:24,423
So, we are more interested normally
on the solution that z3 finds.

165
00:13:24,447 --> 00:13:29,177
And we can get the
solution with get-model.

166
00:13:29,202 --> 00:13:31,863
So, we execute again.

167
00:13:34,328 --> 00:13:35,521
of course, there is a solution,

168
00:13:35,546 --> 00:13:41,200
because we didn't constrain any of
the inputs in the previous solution.

169
00:13:41,201 --> 00:13:44,949
It's always valid for this
kind of operations.

170
00:13:44,960 --> 00:13:48,736
As you can see, z3 goes
always for the trivial solution,

171
00:13:48,761 --> 00:13:49,948
in beginning.

172
00:13:49,973 --> 00:13:53,509
So, we have all the
variables set to zero,

173
00:13:53,510 --> 00:13:56,564
and this is going to be true.

174
00:13:56,588 --> 00:14:02,670
So, what we'll do to change this
is something very intuitive.

175
00:14:02,671 --> 00:14:10,852
If you are writing some kind of code
or testing some kind of binary,

176
00:14:10,877 --> 00:14:15,287
you'll normally know the input
values that you can have.

177
00:14:15,312 --> 00:14:23,626
So, what you can do is just
assert and add a new constraint.

178
00:14:23,651 --> 00:14:27,880
So, to add a new constraint,
we'll use the assert

179
00:14:27,905 --> 00:14:30,021
and we want to constrain
the input value.

180
00:14:30,046 --> 00:14:35,473
So, we are going to constrain
the eax_in for input,

181
00:14:35,498 --> 00:14:38,375
and we can tell z3
whatever we like.

182
00:14:38,400 --> 00:14:45,246
So, let's say that my input
value for eax is 0xffffffff.

183
00:14:45,271 --> 00:14:49,623
So, I added this constraint,

184
00:14:49,648 --> 00:15:01,055
and then, I'll check if it
is satisfied and executing.

185
00:15:01,080 --> 00:15:05,687
And it says that it's still satisfiable.

186
00:15:05,712 --> 00:15:07,840
So. we still have a solution,

187
00:15:07,865 --> 00:15:15,972
but this time, we have a solution
where the input value is 0xffffffff.

188
00:15:15,997 --> 00:15:23,052
So, to get that one, we can use
get-model again and execute.

189
00:15:23,077 --> 00:15:30,869
And then, we see that
we have the constraint

190
00:15:30,894 --> 00:15:35,999
that we said that eax input
needs to be 0xffffffff.

191
00:15:36,011 --> 00:15:41,773
It is valid, and with this value
of eax and this value of ecx,

192
00:15:41,798 --> 00:15:47,900
we can see that the
last value of edx is this one

193
00:15:47,901 --> 00:15:55,250
But this is only one of the possible
solutions that SMT or z3 is finding.

194
00:15:55,275 --> 00:15:57,978
We can find more.

195
00:15:58,003 --> 00:16:04,646
So, for that, we can
constrain now our ecx.

196
00:16:04,671 --> 00:16:10,980
So again, we need to
add another assert.

197
00:16:10,980 --> 00:16:15,130
And this time, we want to
constrain the ecx register.

198
00:16:15,155 --> 00:16:18,610
We can constrain for whatever we want.

199
00:16:18,635 --> 00:16:24,288
So, I will put just 0xeeeeeeee
because I'm very creative.

200
00:16:24,313 --> 00:16:28,946
And then, we are doing the check-sat.

201
00:16:28,971 --> 00:16:34,299
So, executing that again,
it's going to return that yes,

202
00:16:34,324 --> 00:16:36,451
it finds a solution,

203
00:16:38,050 --> 00:16:40,666
also for this last one.

204
00:16:40,691 --> 00:16:44,201
And if it finds a solution, of course,

205
00:16:44,226 --> 00:16:47,266
we want to see which kind
of solution we have.

206
00:16:47,291 --> 00:16:50,663
And this is going to show us again,

207
00:16:50,688 --> 00:16:55,065
just the next possible
solution that z3 finds.

208
00:16:55,090 --> 00:16:59,959
If we get this, we can see
that the eax is to the 0xffs

209
00:16:59,984 --> 00:17:06,605
that we told z3 that the
next constraint for ecx is here.

210
00:17:06,630 --> 00:17:09,731
And then, we get the other values.

211
00:17:09,756 --> 00:17:13,003
But while you're reversing,
or testing or fuzzing,

212
00:17:13,028 --> 00:17:19,999
then, you can use these values when you know
that it isn't what you were looking for,

213
00:17:20,024 --> 00:17:26,985
to add more constraints to your program,
and just keep iterating it.

214
00:17:27,010 --> 00:17:33,093
So that's basically how SMT solver
like z3 or many others work.

215
00:17:33,118 --> 00:17:37,020
And if you want to try a little bit more,

216
00:17:37,044 --> 00:17:40,755
you can even try to
solve the moon challenge.

217
00:17:40,780 --> 00:17:45,530
We are going to solve it
afterwards with Python bindings

218
00:17:45,530 --> 00:17:47,207
and with angr.

219
00:17:47,232 --> 00:17:54,674
But if you want, you can also give
it a try and solve it with SMT.

220
00:17:54,699 --> 00:18:00,798
But keep in mind that as we
were modeling our code here,

221
00:18:00,823 --> 00:18:05,211
you remember that for
five lines of code,

222
00:18:05,236 --> 00:18:09,209
we got seven variables
that we needed to define.

223
00:18:09,221 --> 00:18:13,970
That was just like a snippet of code.

224
00:18:13,995 --> 00:18:18,305
So, if you are trying to do
this for a whole program,

225
00:18:18,330 --> 00:18:21,663
it's going to be a lot of typing.

226
00:18:21,688 --> 00:18:25,094
But yeah, have fun if you want to,

227
00:18:25,105 --> 00:18:32,720
and remember to solve the challenge
that is at the bottom of this video.

