﻿1
00:00:00,160 --> 00:00:02,471
So, let's get started.

2
00:00:02,496 --> 00:00:08,880
I have chosen some exercises,
just some CTF challenges.

3
00:00:08,880 --> 00:00:14,480
They are available on my
keybase public folder,

4
00:00:14,505 --> 00:00:19,116
but they should also be inside
your VM already under exercises.

5
00:00:19,141 --> 00:00:22,696
And you should have all
the binaries there

6
00:00:22,721 --> 00:00:26,179
Just if I change something
or update something,

7
00:00:26,204 --> 00:00:27,332
maybe, I've not changed anythiing.

8
00:00:27,356 --> 00:00:31,021
You also have the link
for angr in GitHub,

9
00:00:31,046 --> 00:00:34,657
where you can also find all the
documentation for the tool,

10
00:00:34,682 --> 00:00:40,027
and they also have a lot of challenges
that they keep in a repository

11
00:00:40,052 --> 00:00:42,988
just for people to try
and learn to use angr.

12
00:00:43,000 --> 00:00:49,602
Other useful tools that I'm
going to be using a lot

13
00:00:49,627 --> 00:00:52,605
when solving these challenges is lldb

14
00:00:53,737 --> 00:00:55,859
If you are more of a gdb person.

15
00:00:55,884 --> 00:00:58,131
You can also instalL gdb,

16
00:00:58,156 --> 00:01:02,895
and then, I normally ask
people to also install gef

17
00:01:02,920 --> 00:01:06,424
because I cannot look at that output.

18
00:01:07,360 --> 00:01:11,482
If you also use lldb,

19
00:01:11,507 --> 00:01:14,616
I like to use lldb inside Python,

20
00:01:15,987 --> 00:01:21,364
and for that, I have this common
handler for lldb that is very useful

21
00:01:21,988 --> 00:01:26,992
when I'm just debugging
or solving challenges.

22
00:01:27,017 --> 00:01:31,144
If you just keep using the Python,

23
00:01:31,156 --> 00:01:34,355
the jupyter-notebooks,

24
00:01:34,380 --> 00:01:39,164
you can just run the
cell and it should work.

25
00:01:41,239 --> 00:01:46,337
If you prefer to install IPython
or just write Python scripts,

26
00:01:46,362 --> 00:01:47,540
it's up to you.

27
00:01:47,565 --> 00:01:53,313
I like to use Python notebooks because
I like to see the output of the tools

28
00:01:53,338 --> 00:01:56,299
and have a kind of a write up.

29
00:01:56,311 --> 00:01:59,360
When I'm solving challenges,
I think it's very practical,

30
00:01:59,360 --> 00:02:04,783
but it's definitely not
the thing for everybody.

31
00:02:04,795 --> 00:02:12,818
As always, I already have here a
small skeleton and how to do it.

32
00:02:12,843 --> 00:02:17,889
If you want to try it before
listening to how I do it,

33
00:02:18,480 --> 00:02:22,346
you are welcome to do it,
so, just press pause.

34
00:02:22,358 --> 00:02:26,080
If you are coming back to
the recording right now,

35
00:02:26,081 --> 00:02:32,107
or if you looked into it
to see the walkthrough,

36
00:02:32,132 --> 00:02:35,771
we are first going to
execute the command file.

37
00:02:35,796 --> 00:02:39,055
You would do it normally
in a command line.

38
00:02:41,172 --> 00:02:49,600
You can execute binaries that
you would do in the terminal,

39
00:02:49,600 --> 00:02:54,705
just using exclamation point
inside the Jupyter Notebook.

40
00:02:54,730 --> 00:02:58,482
So, what I'm doing
here is I just do file.

41
00:02:58,507 --> 00:03:00,810
You can do whatever you prefer.

42
00:03:01,421 --> 00:03:04,104
You can use your preferred
tooling for it.

43
00:03:06,000 --> 00:03:10,241
I like to see which kind of binary
I'm using or looking into.

44
00:03:10,266 --> 00:03:15,761
In this case, we have our
ELF binary that is 32 bit,

45
00:03:16,424 --> 00:03:20,530
and it's not stripped
and dynamically linked.

46
00:03:20,555 --> 00:03:24,598
So, I just look at strings.

47
00:03:24,623 --> 00:03:29,919
That's the very lazy approach
of reverse engineering.

48
00:03:29,944 --> 00:03:34,923
What we see here, for example,
is the name of the challenge

49
00:03:35,425 --> 00:03:38,085
That's the IOLI Crackme level 2.

50
00:03:39,168 --> 00:03:42,236
We have two interesting strings,

51
00:03:42,261 --> 00:03:44,712
the password OK, and
the invalid password.

52
00:03:45,671 --> 00:03:54,207
Everybody that plays a page of CTF or
just like a bit of reverse engineering

53
00:03:54,232 --> 00:03:57,922
know what you're looking at right now.

54
00:03:57,947 --> 00:04:07,640
We'll keep scrolling and we'll
see a lot of GLIBC functions.

55
00:04:07,665 --> 00:04:11,122
So, let's debug it.

56
00:04:11,147 --> 00:04:15,772
As I said before,
I use lldb inside Python.

57
00:04:15,797 --> 00:04:19,751
I can debug directly
from the Jupyter Notebook.

58
00:04:20,421 --> 00:04:22,461
For that, I need the path for my binary.

59
00:04:22,486 --> 00:04:28,902
As I said to you, it's inside the
exercise folder of the virtual machine.

60
00:04:28,927 --> 00:04:37,028
The architecture, as we checked before with "file"
is a 32 bit binary.

61
00:04:37,053 --> 00:04:42,715
So, we need to use the i386.
and then, we set our debugger.

62
00:04:43,170 --> 00:04:46,631
We'll create an instance of a debugger,

63
00:04:46,656 --> 00:04:51,176
and we'll create a target,
that is our binary.

64
00:04:59,280 --> 00:05:01,709
Again, you can use your preferred tooling.

65
00:05:01,734 --> 00:05:04,182
You can do a gdb if you prefer.

66
00:05:04,207 --> 00:05:10,534
All we're doing here right now is
just disassembling the main function.

67
00:05:10,546 --> 00:05:14,157
As we disassemble the main function,

68
00:05:14,182 --> 00:05:19,208
we can see that there is a call for print.

69
00:05:19,672 --> 00:05:26,320
There is another call for printf,
and then, a call for scanf,

70
00:05:30,400 --> 00:05:36,114
and then, another printf call,
and another call for printf.

71
00:05:36,126 --> 00:05:41,614
So, we can remember that we
saw interest in strings.

72
00:05:42,425 --> 00:05:48,483
What we can do is check which kind of
strings we have in these addresses

73
00:05:48,508 --> 00:05:51,471
that are being printed.

74
00:05:51,483 --> 00:06:00,655
To do that, we can just put the
address in my "command-thing" for lldb.

75
00:06:00,680 --> 00:06:12,247
So, the first one that we are
going to check is this one.

76
00:06:13,922 --> 00:06:16,989
So, it's is called
to the function,

77
00:06:17,014 --> 00:06:25,280
and we'll just put an address
that we want to see what's in.

78
00:06:25,919 --> 00:06:30,824
This is our first string
like the IOLI crack me level.

79
00:06:31,440 --> 00:06:33,138
That's very interesting.

80
00:06:33,163 --> 00:06:36,028
So, we'll look into another one,
the next one,

81
00:06:36,054 --> 00:06:41,705
and it's sixty one in the end.

82
00:06:41,730 --> 00:06:43,420
So, we'll go back to our....

83
00:06:49,170 --> 00:06:50,703
.....lldb,

84
00:06:51,921 --> 00:06:55,303
and we'll look what's in
the sixty one address.

85
00:06:55,328 --> 00:06:57,565
And that's asking the password,

86
00:06:57,590 --> 00:07:02,549
and then, we see here our
format string, four digit,

87
00:07:02,574 --> 00:07:06,823
and then, password again,
which is also kind of weird,

88
00:07:06,848 --> 00:07:11,128
but if we take into account
that we have a scanf here,

89
00:07:12,041 --> 00:07:16,240
we could also check what
we have in this address.

90
00:07:28,880 --> 00:07:31,713
It t's not that far from
the others we have.

91
00:07:31,738 --> 00:07:36,160
It's exactly this string
format that we saw before.

92
00:07:36,161 --> 00:07:38,649
It belongs to a scanf.

93
00:07:39,921 --> 00:07:48,001
We have here, compare, and then,
printf and printf for the jump.

94
00:07:48,026 --> 00:07:52,353
So, let's check what's
in these addresses.

95
00:07:52,378 --> 00:07:59,120
So, the first one is also very close
to the address we were before,

96
00:08:01,280 --> 00:08:03,850
and that's password Ok.

97
00:08:03,875 --> 00:08:10,844
So, if the compare works, we are
not jumping to this address here.

98
00:08:10,869 --> 00:08:13,547
We are printing that
the password is right.

99
00:08:13,572 --> 00:08:15,944
We already know what we want to do.

100
00:08:18,880 --> 00:08:20,176
So, let's solve it in angr.

101
00:08:20,201 --> 00:08:24,139
Now that we understand
what the binary is doing,

102
00:08:24,164 --> 00:08:28,664
we are going to import angr
to set up the environment.

103
00:08:31,760 --> 00:08:35,043
And then, we need to create a project.

104
00:08:35,055 --> 00:08:42,907
Creating a project is the way that angr
uses to load a binary into its engine.

105
00:08:43,672 --> 00:08:46,487
And for doing that,
we need to call angr,

106
00:08:46,512 --> 00:08:49,758
and then, we create a project.

107
00:08:49,783 --> 00:08:52,903
And then, we need the binary.

108
00:08:52,928 --> 00:09:01,455
The binary was under exercises,
and that's the number seven angr.

109
00:09:01,480 --> 00:09:05,120
We want the crackme number two.

110
00:09:05,145 --> 00:09:09,304
And then, we can just execute this line.

111
00:09:12,000 --> 00:09:17,671
The next thing that we need to do is to
define the the entry point of the program

112
00:09:18,923 --> 00:09:24,348
and all the paths that is
going to be explored by angr.

113
00:09:25,174 --> 00:09:29,399
So, the first thing that we'll
do is store the entry state.

114
00:09:29,424 --> 00:09:33,325
That is right now before
even starting execution.

115
00:09:33,350 --> 00:09:39,516
So, the entry stage is the
entry point of the binary.

116
00:09:39,528 --> 00:09:46,138
Sometimes, we also call it the path
because it's the start of the path,

117
00:09:46,163 --> 00:09:51,801
but they are just different names
for the same point on the binary.

118
00:09:56,171 --> 00:10:02,415
Before we had at least
two possible paths.

119
00:10:02,440 --> 00:10:04,774
The one that says the
password is okay,

120
00:10:04,799 --> 00:10:09,679
and the one that is saying
password is invalid.

121
00:10:09,704 --> 00:10:17,428
And what we do here is also create a path
group that is just like a symbolic engine.

122
00:10:17,440 --> 00:10:22,951
But sometimes, we simply call it
symbolic engine or symbolic group.

123
00:10:22,976 --> 00:10:28,960
It's just like a name, and sometimes,
we call like it a path group,

124
00:10:29,520 --> 00:10:34,486
because it's a group of all the
possible paths inside the binary.

125
00:10:34,511 --> 00:10:36,344
So, let's execute this.

126
00:10:39,280 --> 00:10:40,400
There we go.

127
00:10:40,425 --> 00:10:43,784
So, we need to decide
what we are looking for,

128
00:10:45,360 --> 00:10:49,268
because we have all possible
paths of the binary

129
00:10:49,280 --> 00:10:52,800
already stored here in
the symbolic group.

130
00:10:53,520 --> 00:10:59,046
If you want to run, we need
to give a direction like,

131
00:10:59,071 --> 00:11:01,286
what is angr looking for?

132
00:11:01,311 --> 00:11:06,708
And we can just look for
everything that is possible.

133
00:11:07,420 --> 00:11:18,260
For example, in this case,
we could just tell angr to run,

134
00:11:18,918 --> 00:11:23,734
and then, we'll define
a lambda function.

135
00:11:23,759 --> 00:11:41,235
And we'll say that pathgroup or the
length of pathgroup is more than one.

136
00:11:41,260 --> 00:11:44,971
So, we know that we are
in in a branching point.

137
00:11:44,996 --> 00:11:51,511
But for this to be true, so we can
redefine this as a lambda function,

138
00:11:51,523 --> 00:11:54,170
which means that the
length needs to be....

139
00:11:58,168 --> 00:12:08,584
.....the length of the active paths inside
the path group needs to be more than one.

140
00:12:09,173 --> 00:12:11,567
And then, we know that we
are in a branching point.

141
00:12:15,200 --> 00:12:19,440
So, we'll run, and then,
you can see angr working,

142
00:12:21,600 --> 00:12:25,873
and the simulation manager
actually found two active paths

143
00:12:25,898 --> 00:12:28,504
after this node where it is right now.

144
00:12:29,120 --> 00:12:32,795
That's interesting because
we know that the place

145
00:12:32,820 --> 00:12:38,161
where the actual password is in our
branch that has two possibilities.

146
00:12:38,172 --> 00:12:43,008
The flag is right and the flag is wrong.

147
00:12:43,033 --> 00:12:49,912
So, at least, we can hope
that we are in the branch

148
00:12:50,918 --> 00:12:54,470
where password is right
vs password is wrong.

149
00:12:54,495 --> 00:12:57,333
To see what is under this node,

150
00:12:57,918 --> 00:13:01,212
we can use the path group, and
then, we'll call all the active.

151
00:13:01,672 --> 00:13:03,861
We know we have two active.

152
00:13:03,886 --> 00:13:08,627
So, we have the active index
zero and active index one.

153
00:13:08,639 --> 00:13:15,042
And to see what is going to happen in
the next line of code after this one,

154
00:13:15,067 --> 00:13:17,317
we can use posix dumps.

155
00:13:17,329 --> 00:13:19,802
So, let's see what's there.

156
00:13:19,827 --> 00:13:28,129
The input necessary to reach the
path number one is the first one

157
00:13:28,154 --> 00:13:34,090
and the input necessary to reach the
second path would be the second one.

158
00:13:34,920 --> 00:13:38,961
We don't know which one is wrong
password and right password.

159
00:13:38,961 --> 00:13:42,128
So, we can just try both of them.

160
00:13:42,153 --> 00:13:46,899
We can start with the first one,
the index number zero.

161
00:13:47,925 --> 00:13:50,875
So, we start this value in the flag,

162
00:13:50,900 --> 00:13:57,120
and then, we use it in the crackme,
and that was the right password.

163
00:13:58,400 --> 00:14:04,896
We can just test for the sake of demo'ing it

164
00:14:04,921 --> 00:14:08,640
with the second one, and then,
it's saying invalid password.

165
00:14:08,640 --> 00:14:12,607
So, we are in the
branching point right now,

166
00:14:12,619 --> 00:14:20,969
and the input necessary to land
into the password is valid.

167
00:14:21,422 --> 00:14:29,064
string would be the first one,
and then, you get the flag.

