﻿1
00:00:00,099 --> 00:00:03,030
What is symbolic execution?

2
00:00:03,030 --> 00:00:07,590
Symbolic execution is just another
way of analyzing binaries.

3
00:00:07,590 --> 00:00:14,045
So, we can analyze binaries with
static analysis, dynamic analysis,

4
00:00:14,070 --> 00:00:16,610
and other shades in between.

5
00:00:16,610 --> 00:00:22,581
What we do with symbolic execution
is basically defining symbols

6
00:00:22,606 --> 00:00:30,000
instead of using concrete inputs with
the symbols that we defined before.

7
00:00:30,000 --> 00:00:33,955
And then, we have the execution
path that is symbolically executed,

8
00:00:33,980 --> 00:00:37,170
but in fact, there is
no execution going on.

9
00:00:37,170 --> 00:00:41,165
Normally, we have our control flow graph,

10
00:00:41,190 --> 00:00:47,643
or we needed to have a syntax
tree like this one after the program

11
00:00:47,668 --> 00:00:55,100
so that we can actually reason for
from every single step on the program.

12
00:00:55,100 --> 00:01:00,597
And all these steps or states of the
program need to be stored somewhere,

13
00:01:00,622 --> 00:01:04,900
just as we were doing
before playing with z3.

14
00:01:04,900 --> 00:01:11,544
And in this case, for example,
we would have all these states

15
00:01:11,569 --> 00:01:15,020
and then, we would ask,
'but I want to read this success'

16
00:01:15,021 --> 00:01:19,094
So, which kind of input do I need to have,
and then, we could reason about it,

17
00:01:19,119 --> 00:01:22,620
and the input would be 32.

18
00:01:22,620 --> 00:01:30,201
And that's how symbolic execution has also
the backtrack algorithm inside.

19
00:01:30,201 --> 00:01:36,900
that you can think about going all
the possible ways down the path,

20
00:01:36,925 --> 00:01:40,906
down the abstract syntax tree,

21
00:01:40,918 --> 00:01:46,070
and then, going back to find out
which kind of input we need.

22
00:01:46,070 --> 00:01:49,857
That's basically what z3 was doing
for us with the Python bindings

23
00:01:49,882 --> 00:01:55,110
in that snippet of five lines.

24
00:01:55,111 --> 00:01:59,494
Lots of considerations need
to be always very visible

25
00:01:59,519 --> 00:02:02,579
when working with symbolic execution.

26
00:02:02,579 --> 00:02:06,139
It's fairly easy to
explode this algorithm

27
00:02:06,151 --> 00:02:09,890
into something very
complex and exponential

28
00:02:09,890 --> 00:02:16,780
like loops or conditional branches, or even
some kind of dynamic memory allocation

29
00:02:16,780 --> 00:02:20,350
where you have symbolic
address in the memory.

30
00:02:20,362 --> 00:02:28,273
It's pretty hard when the computer is not
going to be able to resolve a memory location

31
00:02:28,286 --> 00:02:32,060
that is a symbolic value, for example.

32
00:02:32,061 --> 00:02:34,831
So, why do we use symbolic execution?

33
00:02:34,831 --> 00:02:38,831
And why do we want to
learn symbolic execution?

34
00:02:38,831 --> 00:02:46,190
Basically, in security, I would
use symbolic execution the most

35
00:02:46,215 --> 00:02:50,182
when I have like a path
that I want to trigger.

36
00:02:50,207 --> 00:02:53,390
normally it's a bug or a vulnerability.

37
00:02:53,390 --> 00:02:57,231
And I want to know which
kind of input I need to have

38
00:02:57,243 --> 00:03:03,090
that I can use to trigger that
specific bug that I found.

39
00:03:03,090 --> 00:03:08,435
So, the idea is always to define
the conditions and the path

40
00:03:08,460 --> 00:03:10,450
for reaching that bug,

41
00:03:10,451 --> 00:03:15,346
And ??? from a simple bug, maybe
a security vulnerability.

42
00:03:15,371 --> 00:03:16,920
And with symbolic execution,

43
00:03:16,945 --> 00:03:20,460
as I'm tracking all the possible
constraints and solving them,

44
00:03:20,461 --> 00:03:25,981
step by step, I'm looking to all
permutations that are possible,

45
00:03:25,981 --> 00:03:31,450
and there is no way that, as we talked
before about completeness and soundness,

46
00:03:31,475 --> 00:03:34,892
there is no way that going
to all the permutations

47
00:03:34,905 --> 00:03:40,907
won't find the path that is triggering
that bug if this path exists.

48
00:03:44,470 --> 00:03:49,777
So, we need to remember that
usually it's NP complete,

49
00:03:49,802 --> 00:03:54,917
because we can possibly test all
the inputs that we could have,

50
00:03:54,942 --> 00:03:56,622
for example, in a kernel

51
00:03:56,647 --> 00:04:01,689
And if you were using it for
generating test cases for your fuzzer,

52
00:04:01,714 --> 00:04:05,295
be sure that it's going to generate
with that weird test cases

53
00:04:05,320 --> 00:04:07,975
that are not going to the parser

54
00:04:08,000 --> 00:04:12,049
if you don't sanitize it
in a way that makes sense.

55
00:04:12,074 --> 00:04:17,136
For your application, you need to
handle all the loops and branches

56
00:04:17,161 --> 00:04:20,709
in a way that it makes
sense for your computer.

57
00:04:20,710 --> 00:04:25,381
because the target often contains a lot of
code that is very expensive to analyze,

58
00:04:25,381 --> 00:04:29,291
but it doesn't need to be analyzed.

59
00:04:29,291 --> 00:04:36,448
Sometimes, when you use a fuzzer you know
already which API's you wanted to test

60
00:04:36,473 --> 00:04:38,824
or which functions you want to test,

61
00:04:38,849 --> 00:04:46,366
or at least which kind of assumptions
you can take about a huge source base,

62
00:04:46,391 --> 00:04:55,160
and that need to be connected to where and
why you were using symbolic execution.

