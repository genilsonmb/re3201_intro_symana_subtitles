﻿1
00:00:00,229 --> 00:00:06,799
As we have seen already, angr starts
program whenever we tell it to start.

2
00:00:06,799 --> 00:00:09,301
And wherever we tell it to start.

3
00:00:09,326 --> 00:00:12,539
This is always the first active state.

4
00:00:12,539 --> 00:00:21,400
And then, it executes all the instructions
in its active or non terminated state.

5
00:00:21,400 --> 00:00:25,922
Trying to reach like the branching point in
your case that we were looking for,

6
00:00:26,407 --> 00:00:32,739
or of course, it terminates
in a dead end or it finds.

7
00:00:32,739 --> 00:00:35,379
At every single branching point,

8
00:00:35,404 --> 00:00:39,670
it's going to split the
state into multiple states

9
00:00:39,670 --> 00:00:43,469
and add them all to
this active state set.

10
00:00:43,469 --> 00:00:49,748
As we saw it like before,
we can also use the avoid,

11
00:00:49,773 --> 00:00:56,500
as we just learned to avoid
state explosions, and so on.

12
00:00:56,501 --> 00:01:03,430
It's pretty neat, but it works automatically
if we are using the function explore.

13
00:01:03,455 --> 00:01:06,296
We can always do that by automating it

14
00:01:06,658 --> 00:01:12,682
using the explore function
already built into angr.

15
00:01:12,682 --> 00:01:15,604
We've defined in avoid.

16
00:01:15,629 --> 00:01:19,749
While we are not finding
what we want to find,

17
00:01:19,774 --> 00:01:24,154
it will keep looking and stepping
through all active states.

18
00:01:24,907 --> 00:01:31,243
It's going to check if that's what
we want or not, and break if it is,

19
00:01:31,255 --> 00:01:36,917
and it is going to avoid the avoid
state and mark it for termination.

20
00:01:36,942 --> 00:01:38,830
We don't want it anyway.

21
00:01:38,830 --> 00:01:40,950
So, let's kill it.

22
00:01:40,950 --> 00:01:44,774
At some point, it's going to
remove all termination nodes,

23
00:01:44,799 --> 00:01:49,340
so that we don't have a memory issue.

24
00:01:49,340 --> 00:01:53,653
Sometimes, it can happen that
angr has a lot of states,

25
00:01:53,904 --> 00:01:56,320
and then, the computer really slow.

26
00:01:56,320 --> 00:02:00,740
But we are going to learn
how to deal with that.

27
00:02:00,740 --> 00:02:06,155
So, it's very important if you're doing
symbolic execution the whole time

28
00:02:06,180 --> 00:02:11,265
without injecting any symbols until now.

29
00:02:11,277 --> 00:02:15,450
We just need to care about the
structure of the program.

30
00:02:15,450 --> 00:02:18,982
That's why I was always talking about
the structure of the main function,

31
00:02:19,405 --> 00:02:24,000
the structure of the check
function, and so on.

32
00:02:24,000 --> 00:02:31,000
But the automated way from angr can
only deal with this very simple cases

33
00:02:31,025 --> 00:02:32,569
that we were doing before.

34
00:02:32,569 --> 00:02:39,459
If it gets a little bit more complex,
angr cannot do it alone anymore,

35
00:02:39,459 --> 00:02:43,806
because the input before
was just like an integer

36
00:02:43,831 --> 00:02:49,620
or something directly from
the scanf and that's simple.

37
00:02:50,406 --> 00:02:53,628
If you want, for example,
to interact with the binary.

38
00:02:53,653 --> 00:02:58,730
If the scanf is not the first
thing that we have there.

39
00:02:58,730 --> 00:03:00,441
How can we do that?

40
00:03:00,466 --> 00:03:05,215
That's when symbolic memory
comes into the picture.

41
00:03:05,240 --> 00:03:08,933
We need to create these
symbols ourselves.

42
00:03:08,958 --> 00:03:14,271
We are going restart the program
after the user input is called

43
00:03:14,296 --> 00:03:18,670
and initialize registers
with your symbolic values.

44
00:03:18,671 --> 00:03:25,591
So, with this very simple example,
I will go through how angr is doing it.

45
00:03:25,591 --> 00:03:29,710
We need to mimic the same technique,

46
00:03:29,710 --> 00:03:32,871
but for more complex structures.

47
00:03:32,871 --> 00:03:34,863
So, we have, for example,

48
00:03:34,888 --> 00:03:37,970
these kind of binaries before, right?

49
00:03:37,971 --> 00:03:42,044
We have like an input,
like a scanf kind of thing,

50
00:03:42,068 --> 00:03:43,880
asking for a password.

51
00:03:43,880 --> 00:03:46,308
If the password was correct,
we'll get the flag,

52
00:03:46,333 --> 00:03:47,348
if not, the key.

53
00:03:47,373 --> 00:03:50,219
We'll get 'not right.'

54
00:03:50,220 --> 00:03:53,909
What we are going to
create is like a symbol,

55
00:03:53,909 --> 00:03:55,404
and this is going to be our input.

56
00:03:57,510 --> 00:04:07,158
So, we have here, for example,
the part where we have the input.

57
00:04:08,999 --> 00:04:16,890
Instead of trying to give some kind of code,
we'll use the heart as an input.

58
00:04:16,890 --> 00:04:26,840
So, angr is going to compare and say
that if the heart is equals to OST2rul3z

59
00:04:26,840 --> 00:04:29,013
like our flag,

60
00:04:29,038 --> 00:04:35,531
it's going to be fine and that's
the way that angr does it.

61
00:04:35,531 --> 00:04:40,441
So, the way we are going
to do it now is like this.

62
00:04:40,466 --> 00:04:43,840
We'll analyze the memory of our binary.

63
00:04:43,841 --> 00:04:48,431
So, we have the input, we have registers,
and then, we have other registers.

64
00:04:48,431 --> 00:04:52,031
And then, we have code doing stuff
and we have the flag.

65
00:04:52,031 --> 00:04:56,921
That's basically how
this program works.

66
00:04:56,921 --> 00:05:04,654
So, we are going to get our symbols and
inject it into some kind of register.

67
00:05:07,906 --> 00:05:09,656
What the program is going to do,

68
00:05:11,503 --> 00:05:15,890
as it is not starting here
and waiting for my input,

69
00:05:15,915 --> 00:05:19,833
but jumping directly to
the code doing stuff,

70
00:05:19,858 --> 00:05:22,667
because we don't need the input anymore,

71
00:05:22,692 --> 00:05:27,250
because we are using symbolic values.

72
00:05:27,250 --> 00:05:30,360
So, we can just disconsider this part.

73
00:05:31,237 --> 00:05:35,661
We'll inject our symbolic
values into the register.

74
00:05:35,661 --> 00:05:39,011
So, we'll start from a different state.

75
00:05:39,011 --> 00:05:42,174
We'll create our own state
where all the registers

76
00:05:42,185 --> 00:05:44,319
that we are going to
use for our code,

77
00:05:44,344 --> 00:05:45,656
are symbolic values,

78
00:05:45,681 --> 00:05:48,410
and then, we'll start
the execution here.

79
00:05:48,411 --> 00:05:51,441
So, let's try it ourselves.

